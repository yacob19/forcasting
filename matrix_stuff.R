# 2a)

mat1 <- matrix(data = c(3,-3 ,-4,5 , 1 ,2 ) , nrow = 3 , ncol = 2, byrow = T)
mat2 <-  matrix(data = c(8,10 ,-6,-4 , -6 ,1 ) , nrow = 3 , ncol = 2, byrow = T)

mat_final = 6 * mat1 - 1/2 * mat2
#2b)


rm(list=ls(all=TRUE))


mat1 <- matrix(data = c(5, 1 ,3, 4 , 0,6 ,-1, 2 , -1) , nrow = 3 , ncol = 3, byrow = T)
mat2 <-  matrix(data = c(0,-4,1,5,2,5 ) , nrow = 3 , ncol = 2, byrow = T)

mat_final <- mat1 %*% mat2



#2c)
rm(list=ls(all=TRUE))


mat1 <- matrix(data = c(6, -1 , 3, 0 , 1, 5 ,2, 1),nrow = 2 , ncol = 4, byrow = T)
mat2 <-  matrix(data = c(5, -1,4,0,-2,4,1,-3,4,0,6,4) , nrow = 4 , ncol = 3, byrow = T)

mat_final <- mat1 %*% mat2


#2d

rm(list=ls(all=TRUE))


mat1 <- matrix(data = c(10,5, 1),nrow = 1 , ncol = 3, byrow = T)
mat2 <-  matrix(data = c(2,0,1,1,3,1,5,0,2) , nrow = 3 , ncol = 3, byrow = T)
mat3 <- matrix(data = c(5,-1,-3),nrow = 3 , ncol = 1, byrow = T)

mat_final = mat1  %*% mat2  %*% mat3


